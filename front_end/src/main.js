import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
// import mask from 'vuejs-mask'
import VueTheMask from 'vue-the-mask'
// import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue'
// import { IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue'

Vue.config.productionTip = false

// Vue.use(mask)

Vue.use(Buefy)

Vue.use(VueTheMask)

// Make BootstrapVue available throughout your project
// Vue.use(BootstrapVue)

// Optionally install the BootstrapVue icon components plugin
// Vue.use(IconsPlugin)

// Vue.use(BootstrapVueIcons)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
