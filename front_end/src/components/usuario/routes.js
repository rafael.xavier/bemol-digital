export default [
  {
    path: '/usuarios',
    name: 'usuario.listar',
    component: () => import(
      './views/Listar/Index.vue'
    )
  },
  {
    path: '/usuarios/novo',
    name: 'usuario.novo',
    component: () => import(
      './views/Gerenciar/Index.vue'
    )
  },
  {
    path: '/usuarios/editar/:id',
    name: 'usuario.editar',
    component: () => import(
      './views/Gerenciar/Index.vue'
    )
  }
]
