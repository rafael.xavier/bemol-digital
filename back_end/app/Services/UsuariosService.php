<?php

namespace App\Services;

use App\Repositories\UsuariosRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class UsuariosService
{

    public function __construct(UsuariosRepository $usuariosRepository)
    {
        $this->UsuariosRepository = $usuariosRepository;
    }

    public function listarUsuarios()
    {
        $usuarios = $this->UsuariosRepository->listarUsuarios();
        if (count($usuarios) == 0) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Nenhum registro encontrado.';
            return json_encode($retorno);
        }
        
        return response()->json($usuarios);
    }
    public function buscaUsuario($id)
    {
        $usuarios = $this->UsuariosRepository->buscaUsuario($id);
        
        if (!isset($usuarios->nome)) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Nenhum registro encontrado.';
            return json_encode($retorno);
        }

        return response()->json($usuarios);
    }

    public function isUsuario(Request $request)
    {
        $isUsuario = $this->UsuariosRepository->isUsuario($request);

        if (isset($isUsuario->id)) {
            return $isUsuario;
        } else {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Nenhum registro encontrado.';
            return $retorno;
        }
    }
    
    public function salvarUsuario(Request $request)
    {
        $return = $this->validarDadosUsuario($request, 'insert');
        $return = json_decode($return);
        if ($return->tipo == 'erro') {
            return json_encode($return);
        }

        try {
            $this->UsuariosRepository->salvarUsuario($request);
            $retorno['tipo'] = 'sucesso';
            $retorno['mensagem'] = 'Usuário Cadastrado com Sucesso.';
            return json_encode($retorno);
        } catch (\Throwable $th) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Ops, ocorreu um erro ao tentar salvar os dados do Usuário.';
            return json_encode($retorno);
        }

    }

    public function atualizarUsuario(Request $request)
    {
        $return = $this->validarDadosUsuario($request, 'update');
        $return = json_decode($return);
        if ($return->tipo == 'erro') {
            return json_encode($return);
        }

        $isUsuario = $this->isUsuario($request);

        if (isset($isUsuario->id)) {
            try {
                $atualizarUsuario = $this->UsuariosRepository->atualizarUsuario($request, $isUsuario);

                if ($atualizarUsuario == true) {
                    $retorno['tipo'] = 'sucesso';
                    $retorno['mensagem'] = 'Usuário atualizado com Sucesso.';
                    return json_encode($retorno);
                } else {
                    $retorno['tipo'] = 'erro';
                    $retorno['mensagem'] = 'Problemas ao atualizar o Usuário.';
                    return json_encode($retorno);
                }
            } catch (\Throwable $th) {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = 'Ops, ocorreu um erro ao tentar atualizar os dados do Usuário.';
                return json_encode($retorno);
            }
        } else {
            return json_encode($isUsuario);
        }

    }

    public function alterarSenhaUsuario(Request $request)
    {
        $isUsuario = $this->isUsuario($request);
        if (isset($isUsuario->id)) {
            try {
                if ($isUsuario->senha === md5($request->senha)) {
                    $atualizarUsuario = $this->UsuariosRepository->alterarSenhaUsuario($request, $isUsuario);
                    if ($atualizarUsuario == true) {
                        $retorno['tipo'] = 'sucesso';
                        $retorno['mensagem'] = 'Senha atualizada com Sucesso.';
                        return json_encode($retorno);
                    } else {
                        $retorno['tipo'] = 'erro';
                        $retorno['mensagem'] = 'Problemas ao atualizar a senha.';
                        return json_encode($retorno);
                    }
                } else {
                    $retorno['tipo'] = 'erro';
                    $retorno['mensagem'] = 'Senha incorreta.';
                    return json_encode($retorno);
                }
            } catch (\Throwable $th) {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = 'Ops, ocorreu um erro ao tentar atualizar a senha do Usuário.';
                return json_encode($retorno);
            }
        } else {
            return $isUsuario;
        }
    }

    public function validarDadosUsuario(Request $request, $mode)
    {
        if ($mode == 'insert') {
            $isUsuario = $this->isUsuario($request);
            if (isset($isUsuario->id)) {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = 'CPF já Cadastrado.';
                return json_encode($retorno);
            }

            $emailUsuario = $this->UsuariosRepository->pesquisaUsuarioEmail($request);
            if (isset($emailUsuario->id)) {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = 'E-mail já Cadastrado.';
                return json_encode($retorno);
            }
        }

        $return = $this->validaIdade($request);
        $return = json_decode($return);
        if ($return->tipo == 'erro') {
            return json_encode($return);
        }

        $return = $this->validaCep($request);
        $return = json_decode($return);
        if ($return->tipo == 'erro') {
            return json_encode($return);
        }

        $retorno['tipo'] = 'sucesso';
        return json_encode($retorno);
    }

    public function validaIdade(Request $request)
    {
        // Valida idade
        $data_nasc = date('Y-m-d', strtotime($request->data_nasc));
        $anoAtual = date('Y-m-d');
        $idade = strtotime($anoAtual) - strtotime($data_nasc);
        $years = floor($idade / (365 * 60 * 60 * 24));
        if ($years < 18) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Cadastro não permitido. A idade mínima é 18 anos.';
            return json_encode($retorno);
        }
        if ($request->data_nasc == null) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Idade não informada.';
            return json_encode($retorno);
        }

        $retorno['tipo'] = 'sucesso';
        return json_encode($retorno);
    }

    public function validaCep(Request $request)
    {
        // Se o Cep não for do estado do amazonas não pode se cadastrar.
        if ($request->cep == null) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Cep não informada.';
            return json_encode($retorno);
        } else {
            $viaCep = $this->viaCep($request);
            $viaCep = json_decode($viaCep);
        }
        if (isset($viaCep->uf)) {
            if ($viaCep->uf != 'AM') {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = "Cadastro não permitido. Apenas Cep's do estado do Amazonas são permitidos.";
                return json_encode($retorno);
            }
        } else {
            return json_encode($viaCep);
        }

        $retorno['tipo'] = 'sucesso';
        return json_encode($retorno);
    }

    public function viaCep(Request $request)
    {
        $cep = preg_replace('/[^0-9]/', '', $request->cep);
        try {
            $client = new Client(); //GuzzleHttp\Client
            $request = new \GuzzleHttp\Psr7\Request('GET', 'https://viacep.com.br/ws/' . $cep . '//json/');
            $promise = $client->sendAsync($request)->then(function ($response) {
                $return = json_decode($response->getBody());
                return json_encode($return);
            });
            return $promise->wait();

        } catch (\Throwable $th) {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Ops, tivemos algum problema ao tentar buscar o CEP, tente novamente.';
            return json_encode($retorno);
        }

    }
}
