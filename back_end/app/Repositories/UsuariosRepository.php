<?php

namespace App\Repositories;

use App\Models\Usuarios;
use Illuminate\Http\Request;

class UsuariosRepository
{
    public function isUsuario(Request $request)
    {
        $cpf = preg_replace('/[^0-9]/', '', $request->cpf);
        $usuario = Usuarios::where('cpf', '=', $cpf)
            ->first();
        return $usuario;
    }
    
    public function pesquisaUsuarioEmail(Request $request)
    {
        $usuario = Usuarios::where('email', '=', strtolower($request->email))
            ->first();
        return $usuario;
    }
    
    public function buscaUsuario($id)
    {
        $usuario = Usuarios::find($id);
        return $usuario;
    }

    public function listarUsuarios()
    {
        return Usuarios::all();

    }

    public function salvarUsuario(Request $request)
    {
        $usuario = new Usuarios();
        $usuario->nome = $request->nome;
        $usuario->cpf = preg_replace('/[^0-9]/', '', $request->cpf);
        $usuario->email = strtolower($request->email);
        $usuario->senha = md5($request->senha);
        $usuario->data_nasc = date('Y-m-d', strtotime($request->data_nasc));
        $usuario->ddd = $request->ddd;
        $usuario->tel_cel = $request->tel_cel;
        $usuario->cep = preg_replace('/[^0-9]/', '', $request->cep);
        $usuario->logradouro = $request->logradouro;
        $usuario->complemento = $request->complemento;
        $usuario->bairro = $request->bairro;
        $usuario->localidade = $request->localidade;
        $usuario->uf = $request->uf;
        $usuario->numero = $request->numero;
        $usuario->save();

    }

    public function atualizarUsuario(Request $request, $isUsuario)
    {
        $isUsuario->nome = $request->nome;
        $isUsuario->cpf = preg_replace('/[^0-9]/', '', $request->cpf);
        $isUsuario->email = strtolower($request->email);
        $isUsuario->data_nasc = date('Y-m-d', strtotime($request->data_nasc));
        $isUsuario->ddd = $request->ddd;
        $isUsuario->tel_cel = $request->tel_cel;
        if ($request->senha != null) {
            $usuario->senha = md5($request->senha);
        }
        $isUsuario->cep = preg_replace('/[^0-9]/', '', $request->cep);
        $isUsuario->logradouro = $request->logradouro;
        $isUsuario->complemento = $request->complemento;
        $isUsuario->bairro = $request->bairro;
        $isUsuario->localidade = $request->localidade;
        $isUsuario->uf = $request->uf;
        $isUsuario->numero = $request->numero;
        return $isUsuario->save();
    }

    public function alterarSenhaUsuario(Request $request, $isUsuario)
    {
        $isUsuario->senha = md5($request->novaSenha);
        return $isUsuario->save();
    }
}
