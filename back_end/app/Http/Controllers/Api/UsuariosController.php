<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Services\UsuariosService;

class UsuariosController extends Controller
{
    public function __construct(UsuariosService $usuariosService)
    {
        $this->UsuariosService = $usuariosService;
    }

    public function listarUsuarios()
    {
        return $this->UsuariosService->listarUsuarios();
    }

    public function buscaUsuario($id)
    {
        return $this->UsuariosService->buscaUsuario($id);
    }

    public function salvarUsuario(Request $request)
    {
        return $this->UsuariosService->salvarUsuario($request);
    }

    public function atualizarUsuario(Request $request)
    {
        return $this->UsuariosService->atualizarUsuario($request);
    }

    public function alterarSenhaUsuario(Request $request)
    {
        return $this->UsuariosService->alterarSenhaUsuario($request);
    }
}
