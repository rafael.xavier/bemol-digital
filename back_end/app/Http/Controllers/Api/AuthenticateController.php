<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Usuarios;
use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;

class AuthenticateController extends Controller
{
    public function login()
    {
        if (request(['senha'])) {
            $passwordRequest = request(['senha']);
            $passwordRequest = $passwordRequest['senha'];
            $passwordRequest = md5($passwordRequest);
        } else {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Campo "senha" não informada.';
            return json_encode($retorno);
        }

        if (request(['cpf'])) {
            $cpf = request(['cpf']);
            $cpf = $cpf['cpf'];
        } else {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'Campo "cpf" não informado.';
            return json_encode($retorno);
        }

        if ($cpf != null) {
            $Usuarios = Usuarios::where('cpf', '=', $cpf)
                ->whereNull('deleted_at')
                ->get();

            if (count($Usuarios) > 0) {
                $senha = '';
                $nome = null;

                foreach ($Usuarios as $key => $value) {
                    $senha = $value['senha'];
                    $nome = $value['nome'];
                    unset($value['senha']);
                }

                if ($passwordRequest == $senha) {
                    $payload = JWTFactory::emptyClaims()->addClaims([
                        'sub' => request(['cpf']),
                    ])->make();

                    $token = JWTAuth::encode($payload);

                    $retorno['cpf'] = $cpf;
                    $retorno['nome'] = $nome;
                    $retorno['token'] = (string) $token;

                    return json_encode($retorno);

                } else {
                    $retorno['tipo'] = 'erro';
                    $retorno['mensagem'] = 'Senha inválida.';
                    return json_encode($retorno);
                }

            } else {
                $retorno['tipo'] = 'erro';
                $retorno['mensagem'] = 'CPF não encontrado.';
                return json_encode($retorno);
            }
        } else {
            $retorno['tipo'] = 'erro';
            $retorno['mensagem'] = 'CPF não informado.';
            return json_encode($retorno);
        }
    }
}
