<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nome',100);
            $table->string('cpf', 14)->unique();
            $table->date('data_nasc');
            $table->string('email', 100)->unique();
            $table->string('senha', 200);
            $table->string('ddd', 3)->nullable();
            $table->string('tel_cel', 10)->nullable();
            $table->string('cep', 8);
            $table->string('logradouro', 100);
            $table->string('complemento', 30)->nullable();
            $table->string('bairro', 50);
            $table->string('localidade', 50);
            $table->string('uf', 2);
            $table->integer('numero')->nullable();
            $table->timestamps();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
