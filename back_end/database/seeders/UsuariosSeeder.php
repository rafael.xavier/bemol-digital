<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('usuarios')->insert([
            'nome' => 'Bernardo Igor Guilherme Rocha',
            'cpf' => '87170624206',
            'data_nasc' => '1979-08-22',
            'email' => 'bernardoigorguilhermerocha_@dc4.com.br',
            'senha' => md5('123456'),
            'ddd' => '97',
            'tel_cel' => '997293193',
            'cep' => '69480970',
            'logradouro' => 'Avenida Deputado Danilo Correa',
            'complemento' => 's/c',
            'bairro' => 'Centro',
            'localidade' => 'Tapauá',
            'uf' => 'AM',
            'numero' => 558,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}



