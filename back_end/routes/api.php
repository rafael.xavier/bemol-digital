<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::namespace ('App\Http\Controllers\Api')->group(function () {
    Route::post('login', 'AuthenticateController@login');
});

Route::get('authtoken', function (Request $request) {
    $response = [
        "status" => 200,
        "message" => "Autenticação validada com sucesso.",
        "auth" => true,
    ];

    return json_encode($response);
})->middleware('JwtAuthenticate');

Route::namespace ('App\Http\Controllers\Api')
    ->middleware(['JwtAuthenticate'])
    ->prefix('usuario')->group(function () {

    Route::get('listar', 'UsuariosController@listarUsuarios');
    
    Route::get('buscar/{id}', 'UsuariosController@buscaUsuario');
    
    Route::post('salvar', 'UsuariosController@salvarUsuario');

    Route::put('atualizar', 'UsuariosController@atualizarUsuario');

    Route::put('alterar-senha', 'UsuariosController@alterarSenhaUsuario');
});
