# Projeto Bemol Digital

Projeto foi criado para o desafio proposto pela Bemol Digital.  

## Desafio 

Criar um CRUD para criação de contas de usuários de forma que os 
dados dos sejam relevantes para uma plataforma omnichannel.hannel.

## Solução

Com base em uma plataforma de micro serviços na qual cada um teria seu ambiente próprio e responsável pela mesma entidade em questão, podemos citar algumas características:

- Segurança nos acessos a cada serviço;
- Operação desconjugada;
- Uso de tecnologias como frameworks dos mais atuais(Laravel e vue como exemplos utilizados) tanto para back-end quanto para o front-end;
- Ter a capacidade de responder rápida e adequadamente as requisições adaptando-se de acordo com as circunstâncias.

#### Backend

Elaborado utilizando o frameword Laravel 8 e banco de dados MySQL. Api's protegidas com autenticação por JWT.  

### Frontend

Desenvolvido com VusJS, possui rotas protegidas utilizadas do back-end, sendo possível acessá-las somente via autenticação
ao realizar o login, o mesmo acontece com suas telas de consulta, cadastro e edição.

### Como executar o projeto

Após baixar o repositório, estando na pasta principal do mesmo"bemol_digital", acesse a pasta "back-end" e suba a estrutura composta pelos seguintes containers:

- bemol_digital_back: Composto com Apache e PHP, sendo exposta a porta 8000;
- bemol_digital_front: Usando Nginx com a aplicação front-end(VueJS), sendo exposta a porta 8001;
- bemol_digital_banco: Com o banco de dados MySql.

Através do seguinte comando:
`$ docker-compose up --build `

Após a finalização da criação stack, devemos executar os dois comandos abaixo para que o ambiente esteja pronto para ser usado:

 1. Utilizado para popular o banco com as tabelas necessárias da solução:
   `# docker exec -ti bemol_digital_back php artisan migrate:fresh`
 2. Utilizado para popular a tabela de usuário:
   `# docker exec -it  bemol_digital_back php artisan db:seed --class=UsuariosSeeder`

Agora podemos utilizar a aplicação através do endereço "http://localhost:8001". 
Para fazer o login, devemos utilizar o CPF: 87170624206 e a Senha: 123456
